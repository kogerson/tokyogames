package com.olimpic.TokyoGames.exception;

import java.util.List;

public class BadRequestException extends RuntimeException {

    private List<String> errorList;

    public BadRequestException(List<String> errorList, String message) {
        super(message);
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return this.errorList;
    }
}
