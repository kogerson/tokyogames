package com.olimpic.TokyoGames.repository;

import com.olimpic.TokyoGames.model.Competition;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Repository of competition entity.
 */
public interface CompetitionRepository extends JpaRepository<Competition, Long> {
    /**
     * Will fetch all competitions from database.
     * @param sort
     * @return
     */
    List<Competition> findAll(Sort sort);

    /**
     * Will fetch all competitions filtering it by modalities from database.
     * @param modality
     * @param sort
     * @return
     */
    List<Competition> findByModality(String modality, Sort sort);

    /**
     * Will fetch the conflicted competitions that took place in the same period, in the same place for the same modality.
     * @param modality
     * @param place
     * @param startDate
     * @param endDate
     * @return
     */
    @Query("from Competition where modality = :modality and place = :place and (:startDate between startDate and endDate or :endDate between startDate and endDate)")
    List<Competition> findByConflicted(@Param("modality") String modality, @Param("place") String place, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * Will fetch all competitions that has the game in the same place at the same date.
     * @param place
     * @param startDate
     * @param endDate
     * @return
     */
    List<Competition> findByPlaceAndStartDateGreaterThanEqualAndEndDateLessThanEqual(String place, Date startDate, Date endDate);
}
