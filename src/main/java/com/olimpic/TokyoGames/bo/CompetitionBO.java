package com.olimpic.TokyoGames.bo;

import com.olimpic.TokyoGames.model.Competition;
import com.olimpic.TokyoGames.model.ICompetition;
import com.olimpic.TokyoGames.model.ICompetition.Stage;
import com.olimpic.TokyoGames.repository.CompetitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.data.domain.Sort.Direction.ASC;

/**
 * Business Object of Competition
 */
@Service
public class CompetitionBO {

    private static final int LIMIT_OF_COMPETITION_PER_DAY = 4;
    private static final int COMPETITION_DURATION_IN_MINUTES = 30;
    public static final String DATE_PATTERN = "dd/MM/yyyy HH:mm";

    private CompetitionRepository repository;

    @Autowired
    public CompetitionBO(CompetitionRepository repository) {
        this.repository = repository;
    }

    /**
     * Will fetch the competition data by ID
     * @param id
     * @return Competition data regarding to the ID
     * @throws Exception
     */
    public Competition get(long id) throws Exception {
        Competition competition = repository.findOne(id);

        if (competition == null) {
            throw new Exception(String.format("There are no competition with id %d", id));
        }

        return competition;
    }

    /**
     * Will fetch all competitions. <br>
     * This request can also be filtered by the modality criteria.
     * @param modalityOptional
     * @return List of competitions
     */
    public List<Competition> getAll(Optional<String> modalityOptional) {
        Sort sort = new Sort(ASC, "startDate");

        return modalityOptional.isPresent() ? repository.findByModality(modalityOptional.get(), sort) : repository.findAll(sort);
    }

    /**
     * Add a new competition information into the database.
     * @param competition
     * @return New competition ID.
     * @throws Exception
     */
    public Long add(Competition competition) throws Exception {
        validStartBeforeEndDate(competition.getStartDate(), competition.getEndDate());
        validCompetitionPunctuation(competition.getStage(), competition.getTeam1Point(), competition.getTeam2Point());
        validConflictedCompetitions(competition.getModality(), competition.getPlace(), competition.getStartDate(), competition.getEndDate());
        validLimitOfCompetition(competition.getPlace(), competition.getStartOfDay(), competition.getEndOfDay());
        validCompetitionDuration(competition.checkDuration(COMPETITION_DURATION_IN_MINUTES));

        return repository.save(competition).getId();
    }

    /**
     * Update the competition punctuation according to ID.
     * @param id
     * @param team1Point
     * @param team2Point
     * @return Update competition ID.
     * @throws Exception
     */
    public Long updatePoint(long id, int team1Point, int team2Point) throws Exception {
        Competition competition = get(id);
        competition.setTeam1Point(team1Point);
        competition.setTeam2Point(team2Point);

        validCompetitionPunctuation(competition.getStage(), competition.getTeam1Point(), competition.getTeam2Point());

        return repository.save(competition).getId();
    }

    /**
     * Validate the competitions dates, where if the start date was after the end date, the method will throws an exception.
     * @param startDate
     * @param endDate
     * @throws Exception
     */
    private void validStartBeforeEndDate(Date startDate, Date endDate) throws Exception {
        if(startDate.after(endDate)) {
            throw new Exception("Start date should be before end date");
        }
    }

    /**
     * Validate the competition punctuation, will be throw an exception if: <br>
     * - Team 1 has the punctuation but the team 2 has not.
     * - Team 2 has the punctuation but the team 1 has not.
     * - When both team has the same punctuation, but the stage of the competition is qualifying, quarter finals or round of 16.
     *
     * @param stage
     * @param team1Point
     * @param team2Point
     * @throws Exception
     */
    private void validCompetitionPunctuation(Stage stage, Integer team1Point, Integer team2Point) throws Exception {
        if (team1Point != null || team2Point != null) {
            if (team1Point == null) {
                throw new Exception("Please inform the punctuation of team 1");
            }

            if (team2Point == null) {
                throw new Exception("Please inform the punctuation of team 2");
            }

            if(team1Point.equals(team2Point)) {
                switch (stage) {
                    case QUALIFYING:
                    case QUARTER_FINALS:
                    case ROUND_OF_16:
                        throw new Exception(String.format("The competition must not have the same punctuation at %s stage", stage.getValue()));
                }
            }
        }
    }

    /**
     * Validate the competition duration, where the duration must have at least 30 minutes of game.
     *
     * @param isValid
     * @throws Exception
     */
    private void validCompetitionDuration(boolean isValid) throws Exception {
        if (!isValid) {
            throw new Exception(String.format("The competition must have at least %d minutes of game", COMPETITION_DURATION_IN_MINUTES));
        }
    }

    /**
     * Validate the limit of competitions, where must have maximum of 4 games into the same place and the same date.
     * @param place
     * @param startDate
     * @param endDate
     * @throws Exception
     */
    private void validLimitOfCompetition(String place, Date startDate, Date endDate) throws Exception {
        List<Competition> competitions = repository.findByPlaceAndStartDateGreaterThanEqualAndEndDateLessThanEqual(place, startDate, endDate);
        if (competitions.size() >= LIMIT_OF_COMPETITION_PER_DAY) {
            throw new Exception(String.format("There are more than %d competitions schedules to %s", LIMIT_OF_COMPETITION_PER_DAY, new SimpleDateFormat("dd/MM/yyyy").format(startDate)));
        }
    }

    /**
     * Validate the conflict games, where two games can not take place in the same period, in the same place for the same modality.
     * @param modality
     * @param place
     * @param startDate
     * @param endDate
     * @throws Exception
     */
    private void validConflictedCompetitions(String modality, String place, Date startDate, Date endDate) throws Exception {
        List<Competition> conflictedCompetitions = getConflictedCompetitions(modality, place, startDate, endDate);
        if (!conflictedCompetitions.isEmpty()) {
            Competition firstCompetition = conflictedCompetitions.stream().findFirst().get();
            Competition lastCompetition = conflictedCompetitions.stream().filter(Objects::nonNull).reduce((first, second) -> second).get();
            throw new Exception(String.format("The modality %s must not be marked at %s place from %s until %s", firstCompetition.getModality(), firstCompetition.getPlace(), new SimpleDateFormat(DATE_PATTERN).format(firstCompetition.getStartDate()), new SimpleDateFormat(DATE_PATTERN).format(lastCompetition.getEndDate())));
        }
    }

    /**
     * Fetch the conflicted games that took place in the same period, in the same place for the same modality.
     * @param modality
     * @param place
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    private List<Competition> getConflictedCompetitions(String modality, String place, Date startDate, Date endDate) throws Exception {
        return repository.findByConflicted(modality, place, startDate, endDate);
    }

}
