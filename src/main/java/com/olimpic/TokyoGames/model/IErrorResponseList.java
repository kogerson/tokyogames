package com.olimpic.TokyoGames.model;

import java.util.List;

public interface IErrorResponseList {
    int getErrorCode();

    List<String> getMessages();
}
