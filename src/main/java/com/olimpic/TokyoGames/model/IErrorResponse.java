package com.olimpic.TokyoGames.model;

public interface IErrorResponse {
    int getErrorCode();

    String getMessage();
}
