package com.olimpic.TokyoGames.model;

import java.util.List;

public class ErrorResponseList implements IErrorResponseList {
    private int errorCode;
    private List<String> messages;

    public ErrorResponseList(int errorCode, List<String> message) {
        this.errorCode = errorCode;
        this.messages = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> message) {
        this.messages = message;
    }
}
