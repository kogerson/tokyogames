package com.olimpic.TokyoGames.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Represents the entity of database and the POJO of Competitions.
 */
@Entity
@SequenceGenerator(name = "seq", initialValue = 1, allocationSize = 1)
public class Competition implements ICompetition {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @NotNull(message = "Modality must not be null")
    @NotBlank(message = "Modality must not be blank")
    private String modality;
    @NotNull(message = "Place must not be null")
    @NotBlank(message = "Place must not be blank")
    private String place;
    @NotNull(message = "Start Date must not be null")
    @JsonFormat(pattern="dd/MM/yyyy HH:mm")
    private Date startDate;
    @NotNull(message = "End Date must not be null")
    @JsonFormat(pattern="dd/MM/yyyy HH:mm")
    private Date endDate;
    @NotNull(message = "Team 1 must not be null")
    @NotBlank(message = "Team 1 must not be blank")
    private String team1;
    @NotNull(message =  "Team 2 must not be null")
    @NotBlank(message = "Team 2 must not be blank")
    private String team2;
    @NotNull(message = "Stage must not be null")
    private Stage stage;

    private Integer team1Point;
    private Integer team2Point;

    public Competition() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getModality() {
        return modality;
    }

    public void setModality(String modality) {
        this.modality = modality;
    }

    @Override
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    @Override
    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    @Override
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public Integer getTeam1Point() {
        return team1Point;
    }

    public void setTeam1Point(Integer team1Point) {
        this.team1Point = team1Point;
    }

    @Override
    public Integer getTeam2Point() {
        return team2Point;
    }

    public void setTeam2Point(Integer team2Point) {
        this.team2Point = team2Point;
    }

    /**
     * Will get the start date of day.
     * @return
     */
    @JsonIgnore
    public Date getStartOfDay() {
        LocalDateTime localDateTime = dateToLocalDateTime(startDate);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    /**
     * Will get the end date of day.
     * @return
     */
    @JsonIgnore
    public Date getEndOfDay() {
        LocalDateTime localDateTime = dateToLocalDateTime(endDate);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    private Date localDateTimeToDate(LocalDateTime startOfDay) {
        return Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant());
    }

    private LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
    }

    /**
     * Will check the duration of the games, where should be inform the minimum time in minutes.
     * @param duration
     * @return
     */
    public boolean checkDuration(int duration) {
        long diffInMilliseconds = Math.abs(getEndDate().getTime() - getStartDate().getTime());
        long diffInMinutes = TimeUnit.MINUTES.convert(diffInMilliseconds, TimeUnit.MILLISECONDS);
        return diffInMinutes >= duration;
    }
}
