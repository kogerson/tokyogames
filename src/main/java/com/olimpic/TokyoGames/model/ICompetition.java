package com.olimpic.TokyoGames.model;

import java.util.Date;

public interface ICompetition {

    Long getId();

    String getModality();

    String getPlace();

    Date getStartDate();

    Date getEndDate();

    String getTeam1();

    String getTeam2();

    Stage getStage();

    Integer getTeam1Point();

    Integer getTeam2Point();


    enum Stage {
        QUALIFYING("Qualifying"),
        ROUND_OF_16("Round of 16"),
        QUARTER_FINALS("Quarter Finals"),
        SEMI_FINALS("Semi Finals"),
        FINAL("Final");

        private String value;

        Stage(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
