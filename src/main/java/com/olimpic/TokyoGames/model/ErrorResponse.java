package com.olimpic.TokyoGames.model;

/**
 * This model is responsible to inform the error responses when occurred an exception into the controller
 */
public class ErrorResponse implements IErrorResponse {
    private int errorCode;
    private String message;

    public ErrorResponse() {

    }

    public ErrorResponse(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
