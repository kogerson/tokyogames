package com.olimpic.TokyoGames.controller;

import com.olimpic.TokyoGames.exception.BadRequestException;
import com.olimpic.TokyoGames.model.ErrorResponse;
import com.olimpic.TokyoGames.model.ErrorResponseList;
import com.olimpic.TokyoGames.model.IErrorResponse;
import com.olimpic.TokyoGames.model.IErrorResponseList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<IErrorResponseList> badRequestExceptionHandler(BadRequestException ex) {
        IErrorResponseList errorResponseList = new ErrorResponseList(HttpStatus.BAD_REQUEST.value(), ex.getErrorList());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseList);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<IErrorResponse> exceptionHandler(Exception ex) {
        IErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }
}
