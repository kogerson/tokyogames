package com.olimpic.TokyoGames.controller;

import com.olimpic.TokyoGames.bo.CompetitionBO;
import com.olimpic.TokyoGames.exception.BadRequestException;
import com.olimpic.TokyoGames.model.Competition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/competition")
public class CompetitionController {

    @Autowired
    private CompetitionBO competitionBO;

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<Competition>> get(@RequestParam(value = "modality", required = false) String modality) {
        return ResponseEntity.status(HttpStatus.OK).body(competitionBO.getAll(Optional.ofNullable(modality)));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> save(@RequestBody @Valid Competition competition, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()), "Competition Model Invalid");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(competitionBO.add(competition));
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> update(@PathVariable("id") long competitionId,
                                       @RequestParam("team1") int team1Point,
                                       @RequestParam("team2") int team2Point) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(competitionBO.updatePoint(competitionId, team1Point, team2Point));
    }


}
