package com.olimpic.TokyoGames.bo;

import com.olimpic.TokyoGames.model.Competition;
import com.olimpic.TokyoGames.repository.CompetitionRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static com.olimpic.TokyoGames.model.ICompetition.Stage.*;

@RunWith(MockitoJUnitRunner.class)
public class CompetitionBOTest {
    private static final long EXIST_ID = 1;
    private static final long NOT_EXIST_ID = 2;

    @Mock
    private CompetitionRepository mockRepository;
    @Mock
    private Competition mockCompetition;

    private CompetitionBO competitionBO;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        competitionBO = new CompetitionBO(mockRepository);

        Mockito.when(mockRepository.findOne(Mockito.eq(NOT_EXIST_ID))).thenReturn(null);
        Mockito.when(mockRepository.findOne(Mockito.eq(EXIST_ID))).thenReturn(mockCompetition);
        Mockito.when(mockRepository.findByConflicted(Mockito.anyString(), Mockito.anyString(), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Collections.emptyList());
        Mockito.when(mockRepository.save(Mockito.any(Competition.class))).thenReturn(mockCompetition);

        Date startDate = Date.from(LocalDateTime.of(2011, 11, 11, 10, 00).atZone(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(LocalDateTime.of(2011, 11, 11, 11, 00).atZone(ZoneId.systemDefault()).toInstant());

        Mockito.when(mockCompetition.getStartDate()).thenReturn(startDate);
        Mockito.when(mockCompetition.getEndDate()).thenReturn(endDate);
        Mockito.when(mockCompetition.getStage()).thenReturn(QUALIFYING);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(null);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(null);
        Mockito.when(mockCompetition.checkDuration(Mockito.anyInt())).thenReturn(true);

    }

    @Test
    public void should_get_competition_by_id() throws Exception {
        Assert.assertNotNull(competitionBO.get(EXIST_ID));
    }

    @Test(expected = Exception.class)
    public void should_throws_exception_when_informed_an_id_that_not_exists_on_database() throws Exception {
        competitionBO.get(NOT_EXIST_ID);
    }

    @Test
    public void should_find_all_competitions_without_filter() {
        competitionBO.getAll(Optional.empty());

        Mockito.verify(mockRepository).findAll(Mockito.any(Sort.class));
    }

    @Test
    public void should_find_all_competitions_when_filter_is_null() {
        competitionBO.getAll(Optional.ofNullable(null));

        Mockito.verify(mockRepository).findAll(Mockito.any(Sort.class));
    }

    @Test
    public void should_find_competitions_by_filter() {
        competitionBO.getAll(Optional.of("mock criteria"));

        Mockito.verify(mockRepository).findByModality(Mockito.anyString(), Mockito.any(Sort.class));
    }

    @Test(expected = Exception.class)
    public void should_throws_an_error_when_informed_start_date_after_end_date() throws Exception {
        Date startDate = Date.from(LocalDateTime.of(2011, 11, 11, 12, 00).atZone(ZoneId.systemDefault()).toInstant());
        Mockito.when(mockCompetition.getStartDate()).thenReturn(startDate);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throws_an_error_when_informed_end_date_before_start_date() throws Exception {
        Date endDate = Date.from(LocalDateTime.of(2011, 11, 11, 9, 00).atZone(ZoneId.systemDefault()).toInstant());
        Mockito.when(mockCompetition.getEndDate()).thenReturn(endDate);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_error_when_informed_just_the_punctuation_of_team_1() throws Exception {
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(null);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_error_when_informed_just_the_punctuation_of_team_2() throws Exception {
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(null);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_qualifying() throws Exception {
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_quarter_finals() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(QUARTER_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_round_of_16() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(ROUND_OF_16);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
    }

    @Test
    public void should_save_when_informed_the_same_punctuation_when_the_stage_was_semi_finals() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(SEMI_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_save_when_informed_the_same_punctuation_when_the_stage_was_final() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(FINAL);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.add(mockCompetition);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_error_when_conflicted_competitions_date_time() throws Exception {
        Mockito.when(mockRepository.findByConflicted(Mockito.anyString(), Mockito.anyString(), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Arrays.asList(mockCompetition));

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_error_when_occurred_more_than_4_competitions_in_the_same_day() throws Exception {
        Mockito.when(mockRepository.findByPlaceAndStartDateGreaterThanEqualAndEndDateLessThanEqual(Mockito.anyString(), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Arrays.asList(mockCompetition, mockCompetition, mockCompetition, mockCompetition, mockCompetition));
        competitionBO.add(mockCompetition);
    }

    @Test
    public void should_save_the_competition_when_informed_4_competitions() throws Exception {
        Mockito.when(mockRepository.findByPlaceAndStartDateGreaterThanEqualAndEndDateLessThanEqual(Mockito.anyString(), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Arrays.asList(mockCompetition, mockCompetition, mockCompetition, mockCompetition));
        competitionBO.add(mockCompetition);

        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_save_the_competition_when_informed_less_than_4_competitions() throws Exception {
        Mockito.when(mockRepository.findByPlaceAndStartDateGreaterThanEqualAndEndDateLessThanEqual(Mockito.anyString(), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Arrays.asList(mockCompetition, mockCompetition, mockCompetition));
        competitionBO.add(mockCompetition);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_error_when_competition_has_less_than_30_minutes_of_duration() throws Exception {
        Mockito.when(mockCompetition.checkDuration(Mockito.anyInt())).thenReturn(false);

        competitionBO.add(mockCompetition);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_qualifying_on_update() throws Exception {
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.updatePoint(EXIST_ID, 1, 1);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_quarter_finals_on_udpate() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(QUARTER_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.updatePoint(EXIST_ID, 1, 1);
    }

    @Test(expected = Exception.class)
    public void should_throw_error_when_informed_the_same_punctuation_when_the_stage_was_round_of_16_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(ROUND_OF_16);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.updatePoint(EXIST_ID, 1, 1);
    }

    @Test
    public void should_save_when_informed_the_same_punctuation_when_the_stage_was_semi_finals_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(SEMI_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.updatePoint(EXIST_ID, 1, 1);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_save_when_informed_the_same_punctuation_when_the_stage_was_final_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(FINAL);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(1);

        competitionBO.updatePoint(EXIST_ID, 1, 1);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_throw_error_when_informed_the_different_punctuation_when_the_stage_was_qualifying_on_update() throws Exception {
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(2);

        competitionBO.updatePoint(EXIST_ID, 1, 2);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_throw_error_when_informed_the_different_punctuation_when_the_stage_was_quarter_finals_on_udpate() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(QUARTER_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(2);

        competitionBO.updatePoint(EXIST_ID, 1, 2);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_throw_error_when_informed_the_different_punctuation_when_the_stage_was_round_of_16_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(ROUND_OF_16);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(2);

        competitionBO.updatePoint(EXIST_ID, 1, 2);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_save_when_informed_the_different_punctuation_when_the_stage_was_semi_finals_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(SEMI_FINALS);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(2);

        competitionBO.updatePoint(EXIST_ID, 1, 2);
        Mockito.verify(mockRepository).save(mockCompetition);
    }

    @Test
    public void should_save_when_informed_the_different_punctuation_when_the_stage_was_final_on_update() throws Exception {
        Mockito.when(mockCompetition.getStage()).thenReturn(FINAL);
        Mockito.when(mockCompetition.getTeam1Point()).thenReturn(1);
        Mockito.when(mockCompetition.getTeam2Point()).thenReturn(2);

        competitionBO.updatePoint(EXIST_ID, 1, 2);
        Mockito.verify(mockRepository).save(mockCompetition);
    }
}
