# Olimpic Tokyo Games
This project was produced to create an Rest API to control and manage the olimpic games from Tokyo.

# Project Setup
## Java
At first, please download the Java JDK 8 clicking [here](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html).
After downloaded it, please setup the path according to your OS.
Then execute the command `java --version` into your prompt command to ensure that the JDK was configured correctly.

### Windows
- Right-click on 'My Computer' and select 'Properties'.
- Click the 'Environment variables' button under the 'Advanced' tab.
- Now, alter the 'Path' variable so that it also contains the path to the Java executable.

### LINUX, UNIX, Solaris, FreeBSD
Environment variable PATH should be set to point to where the Java binaries have been installed.
Example, if you use _bash_ as your shell, then add the following line to the end of your '.bashrc: export PATH = /path/to/java:$PATH'.

### Mac OS X
Set the variable to /usr/libexec/java_home, just export `$JAVA_HOME` in file `~/.bash_profile` or `~/.profile`. 

## IDE
Please install a Java IDE to start to programming into this project.
You can use: 
- Eclipse
- IntelliJ

Or which IDE you want.

## GIT
Please download the GIT click [here](https://git-scm.com/).
Install it into your machine.

## Downloading the project
Access the project repository clicking [here](https://bitbucket.org/kogerson/tokyogames/overview).
Then clone the project from the git command:

`git clone [Project Clone Link]`.

## Starting Project
To run the application, please execute the command:

## Gradle
```
./gradlew build && java -jar build/libs/TokyoGames-1.0.0-SNAPSHOT.jar
```

## Maven
```
mvn package && java -jar target/TokyoGames-1.0.0-SNAPSHOT.jar
```

# Tests

The methodology that was adopted to this project was the TDD.

Unit tests regarding to rest API logic can be found into `tests` package. 

The integration tests was made using the `Postman`.
The source code corresponding to those tests can be found into `tests/resoures/TokyoGames.postman_collection.json` file. 